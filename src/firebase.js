import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

firebase.initializeApp({
    apiKey: "AIzaSyDPZR1duj1QP63zlBTQiNpXnDBnnq0EwEw",
    authDomain: "chat-vue-86b27.firebaseapp.com",
    databaseURL: "https://chat-vue-86b27.firebaseio.com",
    projectId: "chat-vue-86b27",
    storageBucket: "chat-vue-86b27.appspot.com",
    messagingSenderId: "325201466828",
    appId: "1:325201466828:web:fa381bd34f0cf02e273969"
});

export const db = firebase.firestore();

export const auth = firebase.auth();

export const storage = firebase.storage();
