
import VueRouter from 'vue-router';

const router = new VueRouter({
    routes:[
        {
            path: '/',
            name: 'home',
            component: () => import('../src/views/Home')
        },
        {
            path: '/chats/:id',
            name: 'chat',
            component: () => import('../src/views/ChatRoom')
        }
    ],
    mode: 'history'
})

export default router;